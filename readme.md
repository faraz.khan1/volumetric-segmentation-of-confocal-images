# Volumetric Segmentation of Cell Cycle Markers in Confocal Images using Machine Learning and Deep Learning

Dr. Faraz Ahmad Khan,  Dr.Ute Voss, Dr. Michael P Pound and Dr. Andrew P French

Online repository for the above titled paper. 

  

# Orthogonal Views

  - Source code in respective folders.
  - Plugin jar file located in "Orthogonal Views\src\main\java\com\mikepound"

# Confocal Dataset

Full dataset uploaded as individual timepoints with their respective generated ground truth volumes. The dataset is uploaded as hdf5 files in the folder **Confocal Dataset HDF5**. 
Within each hdf5 file:
 - Raw volume stored in \raw
 - ground truth stored in \label

Raw dataset as *.tif files along with expert annotation uploaded to folder **Confocal Dataset TIFF**
