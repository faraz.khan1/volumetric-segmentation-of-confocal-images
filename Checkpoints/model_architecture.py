"""
Architecture of model, written in Pytorch
"""

UNet3D(
  (encoders): ModuleList(
    (0): Encoder(
      (double_conv): DoubleConv(
        (conv1): Conv3d(1, 32, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
        (relu1): ReLU(inplace)
        (norm1): GroupNorm3d()
        (conv2): Conv3d(32, 64, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
        (relu2): ReLU(inplace)
        (norm2): GroupNorm3d()
      )
    )
    (1): Encoder(
      (max_pool): MaxPool3d(kernel_size=(2, 2, 2), stride=(2, 2, 2), padding=1, dilation=1, ceil_mode=False)
      (double_conv): DoubleConv(
        (conv1): Conv3d(64, 64, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
        (relu1): ReLU(inplace)
        (norm1): GroupNorm3d()
        (conv2): Conv3d(64, 128, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
        (relu2): ReLU(inplace)
        (norm2): GroupNorm3d()
      )
    )
    (2): Encoder(
      (max_pool): MaxPool3d(kernel_size=(2, 2, 2), stride=(2, 2, 2), padding=1, dilation=1, ceil_mode=False)
      (double_conv): DoubleConv(
        (conv1): Conv3d(128, 128, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
        (relu1): ReLU(inplace)
        (norm1): GroupNorm3d()
        (conv2): Conv3d(128, 256, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
        (relu2): ReLU(inplace)
        (norm2): GroupNorm3d()
      )
    )
    (3): Encoder(
      (max_pool): MaxPool3d(kernel_size=(2, 2, 2), stride=(2, 2, 2), padding=1, dilation=1, ceil_mode=False)
      (double_conv): DoubleConv(
        (conv1): Conv3d(256, 256, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
        (relu1): ReLU(inplace)
        (norm1): GroupNorm3d()
        (conv2): Conv3d(256, 512, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
        (relu2): ReLU(inplace)
        (norm2): GroupNorm3d()
      )
    )
  )
  (decoders): ModuleList(
    (0): Decoder(
      (double_conv): DoubleConv(
        (conv1): Conv3d(768, 256, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
        (relu1): ReLU(inplace)
        (norm1): GroupNorm3d()
        (conv2): Conv3d(256, 256, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
        (relu2): ReLU(inplace)
        (norm2): GroupNorm3d()
      )
    )
    (1): Decoder(
      (double_conv): DoubleConv(
        (conv1): Conv3d(384, 128, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
        (relu1): ReLU(inplace)
        (norm1): GroupNorm3d()
        (conv2): Conv3d(128, 128, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
        (relu2): ReLU(inplace)
        (norm2): GroupNorm3d()
      )
    )
    (2): Decoder(
      (double_conv): DoubleConv(
        (conv1): Conv3d(192, 64, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
        (relu1): ReLU(inplace)
        (norm1): GroupNorm3d()
        (conv2): Conv3d(64, 64, kernel_size=(3, 3, 3), stride=(1, 1, 1), padding=(1, 1, 1))
        (relu2): ReLU(inplace)
        (norm2): GroupNorm3d()
      )
    )
  )
  (final_conv): Conv3d(64, 1, kernel_size=(1, 1, 1), stride=(1, 1, 1))
  (final_activation): Sigmoid()
)