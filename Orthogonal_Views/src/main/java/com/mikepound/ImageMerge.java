package com.mikepound;

import ij.*;
import ij.plugin.PlugIn;
import ij.process.*;
import ij.gui.*;

public class ImageMerge {

	private ImagePlus imp; ImagePlus[] image = new ImagePlus[4];

	ImageStack rgb; int w,h,d; boolean delete;

    static int  G=0;
	static int  r=1;
	static int  g=2;
	static int  b=3;
	
	public ImagePlus run(ImagePlus ip1, ImagePlus ip2) {
		if (!CheckStacks(ip1, ip2)) {
			IJ.error("Cannot merge these stacks, incorrect size or shape.");
			return null;
		}

		ImagePlus merged = IJ.createHyperStack("merged", ip1.getWidth(), ip1.getHeight(), 2, ip1.getNSlices(), ip1.getNFrames(), ip1.getBitDepth());
		merged.setCalibration(ip1.getCalibration());
		ImageStack is = ip1.getImageStack();
		ImageStack is2 = ip2.getImageStack();
		ImageStack out = merged.getImageStack();

		float[] vx = new float[merged.getWidth() * merged.getHeight()];
		

		for (int sx = 0; sx < is.getSize(); sx++) {
			vx = is.getVoxels(0,0,sx,ip1.getWidth(), ip1.getHeight(), 1,null);
			out.setVoxels(0,0,sx*2,ip1.getWidth(), ip1.getHeight(), 1,vx);
		}
		
		for (int sx = 0; sx < is2.getSize(); sx++) {
			vx = is2.getVoxels(0,0,sx,ip2.getWidth(), ip2.getHeight(), 1,null);
			out.setVoxels(0,0,sx*2+1,ip1.getWidth(), ip1.getHeight(), 1,vx);
		}
	
		return merged;
	}

	public boolean CheckStacks(ImagePlus ip1, ImagePlus ip2){
		if (ip1.getWidth() != ip1.getWidth()
	     || ip1.getHeight() != ip1.getHeight()) {
			return false;
		}
		
		if (ip1.getNChannels() != 1 || ip2.getNChannels() != 1) {
			return false;
		}
		
		if (ip1.getNSlices() != ip2.getNSlices()) {
			return false;
		}
		
		if (ip1.getNFrames() != ip2.getNFrames()) {
			return false;
		}
			
		return true;
	}
}

