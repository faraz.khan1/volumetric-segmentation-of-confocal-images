package com.mikepound;

import ij.plugin.*;
import ij.*;
import ij.gui.*;
import ij.io.OpenDialog;
import ij.io.SaveDialog;
import ij.measure.*;
import ij.process.*;
import java.awt.*;
import java.awt.image.*;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.*;

/**
 * This plugin projects dynamically orthogonal XZ and YZ views of a stack. The
 * output images are calibrated, which allows measurements to be performed more
 * easily.
 * 
 * Many thanks to Jerome Mutterer for the code contributions and testing. Thanks
 * to Wayne Rasband for the code that properly handles the image magnification.
 * 
 * @author Dimiter Prodanov
 */
public class Orthogonal_Views
		implements PlugIn, MouseListener, MouseMotionListener, KeyListener, ActionListener, ImageListener,
		WindowListener, AdjustmentListener, MouseWheelListener, FocusListener, Runnable, CommandListener {

	private class Point4D {
		public int x, y, z, t;

		public Point4D(int X, int Y, int Z, int T) {
			x = X;
			y = Y;
			z = Z;
			t = T;
		}

		@Override
		public String toString() {
			return String.format("(%d, %d, %d, %d)", x, y, z, t);
		}
	}

	private ImageWindow win;
	private ImagePlus xy_stack;
	private Overlay xy_overlay;
	private boolean rgb;
	private ImageStack imageStack;
	private boolean hyperstack;
	private int currentChannel, currentFrame, currentMode;
	private ImageCanvas canvas;

	private static boolean sticky = true;
	private static int xzID, yzID;
	private static Orthogonal_Views instance;
	private ImagePlus xz_image, yz_image;
	private Overlay xz_overlay, yz_overlay;
	/** ImageProcessors for the xz and yz images */
	private ImageProcessor fp1, fp2;
	private double ax, ay, az;

	private int xyX, xyY;
	private Calibration cal = null, cal_xz = new Calibration(), cal_yz = new Calibration();

	private Color color = Roi.getColor();
	private double min, max;
	private Dimension screen = IJ.getScreenSize();
	private Point crossLoc;
	private boolean firstTime = true;
	private static int previousID, previousX, previousY;
	private Rectangle startingSrcRect;
	private boolean done;
	private boolean initialized;
	private boolean sliceSet;
	private Thread thread;
	Map<Integer, ArrayList<Point4D>> annotationLists;

	public void run(String arg) {
		xy_stack = IJ.getImage();
		xy_overlay = new Overlay();
		xy_stack.setOverlay(xy_overlay);
		boolean isStack = xy_stack.getStackSize() > 1;
		hyperstack = xy_stack.isHyperStack();
		if ((hyperstack || xy_stack.isComposite()) && xy_stack.getNSlices() <= 1)
			isStack = false;
		if (instance != null) {
			if (xy_stack == instance.xy_stack) {
				instance.dispose();
				return;
			} else if (isStack) {
				instance.dispose();
				if (IJ.isMacro())
					IJ.wait(1000);
			} else {
				ImageWindow win = instance.xy_stack != null ? instance.xy_stack.getWindow() : null;
				if (win != null)
					win.toFront();
				return;
			}
		}
		if (!isStack) {
			IJ.error("Othogonal Views", "This command requires a stack, or a hyperstack with Z>1.");
			return;
		}

		// Init time-point array lists.
		int t = xy_stack.getNFrames();
		annotationLists = new HashMap<Integer, ArrayList<Point4D>>();
		for (int i = 0; i < t; i++)
			annotationLists.put(i, new ArrayList<Point4D>());

		yz_image = WindowManager.getImage(yzID);

		rgb = xy_stack.getBitDepth() == 24 || hyperstack;
		int yzBitDepth = hyperstack ? 24 : xy_stack.getBitDepth();
		if (yz_image == null || yz_image.getHeight() != xy_stack.getHeight() || yz_image.getBitDepth() != yzBitDepth)
			yz_image = new ImagePlus();

		yz_overlay = new Overlay();
		yz_image.setOverlay(yz_overlay);

		xz_image = WindowManager.getImage(xzID);
		if (xz_image == null || xz_image.getWidth() != xy_stack.getWidth() || xz_image.getBitDepth() != yzBitDepth)
			xz_image = new ImagePlus();

		xz_overlay = new Overlay();
		xz_image.setOverlay(xz_overlay);

		instance = this;
		int mode = xy_stack.getCompositeMode();
		ImageProcessor ip = mode == IJ.COMPOSITE ? new ColorProcessor(xy_stack.getImage()) : xy_stack.getProcessor();
		min = ip.getMin();
		max = ip.getMax();
		cal = this.xy_stack.getCalibration();
		double calx = cal.pixelWidth;
		double caly = cal.pixelHeight;
		double calz = cal.pixelDepth;
		ax = 1.0;
		ay = caly / calx;
		az = calz / calx;
		
		win = xy_stack.getWindow();
		canvas = win.getCanvas();
		addListeners(canvas);

		xy_stack.deleteRoi();
		Rectangle r = canvas.getSrcRect();
		if (xy_stack.getID() == previousID)
			crossLoc = new Point(previousX, previousY);
		else
			crossLoc = new Point(r.x + r.width / 2, r.y + r.height / 2);
		imageStack = getStack();
		calibrate();
		if (createProcessors(imageStack)) {
			if (ip.isColorLut() || ip.isInvertedLut()) {
				ColorModel cm = ip.getColorModel();
				fp1.setColorModel(cm);
				fp2.setColorModel(cm);
			}
			thread = new Thread(this, "Orthogonal Views");
			thread.start();
			IJ.wait(100);
			update();
		} else
			dispose();
	}

	private ImageStack getStack() {
		if (xy_stack.isHyperStack()) {
			int slices = xy_stack.getNSlices();
			int c = xy_stack.getChannel();
			int z = xy_stack.getSlice();
			int t = xy_stack.getFrame();
			int mode = xy_stack.getCompositeMode();
			rgb = mode == IJ.COMPOSITE;
			ColorModel cm = rgb ? null : xy_stack.getProcessor().getColorModel();
			// IJ.log("getStack; "+c+" "+currentChannel+" "+fp1);
			if (cm != null && fp1 != null && fp1.getBitDepth() != 24) {
				fp1.setColorModel(cm);
				fp2.setColorModel(cm);
			}
			ImageStack stack = xy_stack.getStack();
			ImageStack stack2 = new ImageStack(xy_stack.getWidth(), xy_stack.getHeight());
			for (int i = 1; i <= slices; i++) {
				if (rgb) {
					xy_stack.setPositionWithoutUpdate(c, i, t);
					stack2.addSlice(null, new ColorProcessor(xy_stack.getImage()));
				} else {
					int index = xy_stack.getStackIndex(c, i, t);
					stack2.addSlice(null, stack.getProcessor(index));
				}
			}
			if (rgb)
				xy_stack.setPosition(c, z, t);
			currentChannel = c;
			currentFrame = t;
			currentMode = mode;
			return stack2;
		} else
			return xy_stack.getStack();
	}

	private void addListeners(ImageCanvas canvas) {
		canvas.addMouseListener(this);
		canvas.addMouseMotionListener(this);
		canvas.addKeyListener(this);
		win.addWindowListener(this);
		win.addMouseWheelListener(this);
		win.addFocusListener(this);
		ImagePlus.addImageListener(this);
		Executer.addCommandListener(this);
	}

	private void calibrate() {
		String unit = cal.getUnit();
		double o_depth = cal.pixelDepth;
		double o_height = cal.pixelHeight;
		double o_width = cal.pixelWidth;
		cal_yz.setUnit(unit);

		cal_yz.pixelWidth = o_depth / az;
		cal_yz.pixelHeight = o_height;

		yz_image.setCalibration(cal_yz);
		yz_image.setIJMenuBar(false);
		cal_xz.setUnit(unit);
		cal_xz.pixelWidth = o_width;
		cal_xz.pixelHeight = o_depth / az;
		xz_image.setCalibration(cal_xz);
		xz_image.setIJMenuBar(false);
	}

	// {{ Update Views
	void updateViews(Point p, ImageStack is) {
		if (fp1 == null)
			return;
		updateXZView(p, is);

		double arat = az / ax;
		int width2 = fp1.getWidth();
		int height2 = (int) Math.round(fp1.getHeight() * az);
		if (width2 != fp1.getWidth() || height2 != fp1.getHeight()) {
			fp1.setInterpolate(true);
			ImageProcessor sfp1 = fp1.resize(width2, height2);
			if (!rgb)
				sfp1.setMinAndMax(min, max);
			xz_image.setProcessor("XZ " + p.y, sfp1);
		} else {
			if (!rgb)
				fp1.setMinAndMax(min, max);
			xz_image.setProcessor("XZ " + p.y, fp1);
		}

		updateZYView(p, is);

		width2 = (int) Math.round(fp2.getWidth() * az);
		height2 = fp2.getHeight();
		String title = "YZ ";

		if (width2 != fp2.getWidth() || height2 != fp2.getHeight()) {
			fp2.setInterpolate(true);
			ImageProcessor sfp2 = fp2.resize(width2, height2);
			if (!rgb)
				sfp2.setMinAndMax(min, max);
			yz_image.setProcessor(title + p.x, sfp2);
		} else {
			if (!rgb)
				fp2.setMinAndMax(min, max);
			yz_image.setProcessor(title + p.x, fp2);
		}

		calibrate();
		if (yz_image.getWindow() == null) {
			yz_image.show();
			ImageCanvas ic = yz_image.getCanvas();
			ic.addKeyListener(this);
			ic.addMouseListener(this);
			ic.setCustomRoi(true);
			yzID = yz_image.getID();
		} else {
			ImageCanvas ic = yz_image.getWindow().getCanvas();
			ic.addMouseListener(this);
			ic.setCustomRoi(true);
		}
		if (xz_image.getWindow() == null) {
			xz_image.show();
			ImageCanvas ic = xz_image.getCanvas();
			ic.addKeyListener(this);
			ic.addMouseListener(this);
			ic.setCustomRoi(true);
			xzID = xz_image.getID();
		} else {
			ImageCanvas ic = xz_image.getWindow().getCanvas();
			ic.addMouseListener(this);
			ic.setCustomRoi(true);
		}

	}

	void arrangeWindows(boolean sticky) {
		ImageWindow xyWin = xy_stack.getWindow();
		if (xyWin == null)
			return;
		Point loc = xyWin.getLocation();
		if ((xyX != loc.x) || (xyY != loc.y)) {
			xyX = loc.x;
			xyY = loc.y;
			ImageWindow yzWin = null;
			long start = System.currentTimeMillis();
			while (yzWin == null && (System.currentTimeMillis() - start) <= 2500L) {
				yzWin = yz_image.getWindow();
				if (yzWin == null)
					IJ.wait(50);
			}
			if (yzWin != null)
				yzWin.setLocation(xyX + xyWin.getWidth(), xyY);
			ImageWindow xzWin = null;
			start = System.currentTimeMillis();
			while (xzWin == null && (System.currentTimeMillis() - start) <= 2500L) {
				xzWin = xz_image.getWindow();
				if (xzWin == null)
					IJ.wait(50);
			}
			if (xzWin != null)
				xzWin.setLocation(xyX, xyY + xyWin.getHeight());
			if (firstTime) {
				xy_stack.getWindow().toFront();
				if (!sliceSet) {
					if (hyperstack)
						xy_stack.setPosition(xy_stack.getChannel(), xy_stack.getNSlices() / 2, xy_stack.getFrame());
					else
						xy_stack.setSlice(xy_stack.getNSlices() / 2);
				}
				firstTime = false;
			}
		}
	}

	boolean createProcessors(ImageStack is) {
		ImageProcessor ip = is.getProcessor(1);
		int width = is.getWidth();
		int height = is.getHeight();
		int ds = is.getSize();
		double arat = 1.0;// az/ax;
		double brat = 1.0;// az/ay;
		int za = (int) (ds * arat);
		int zb = (int) (ds * brat);

		if (ip instanceof FloatProcessor) {
			fp1 = new FloatProcessor(width, za);
			fp2 = new FloatProcessor(zb, height);
			return true;
		}

		if (ip instanceof ByteProcessor) {
			fp1 = new ByteProcessor(width, za);
			fp2 = new ByteProcessor(zb, height);
			return true;
		}

		if (ip instanceof ShortProcessor) {
			fp1 = new ShortProcessor(width, za);
			fp2 = new ShortProcessor(zb, height);
			return true;
		}

		if (ip instanceof ColorProcessor) {
			fp1 = new ColorProcessor(width, za);
			fp2 = new ColorProcessor(zb, height);
			return true;
		}

		return false;
	}

	void updateXZView(Point p, ImageStack is) {
		int width = is.getWidth();
		int size = is.getSize();
		ImageProcessor ip = is.getProcessor(1);

		int y = p.y;
		// XZ
		if (ip instanceof ShortProcessor) {
			short[] newpix = new short[width * size];
			for (int i = 0; i < size; i++) {
				Object pixels = is.getPixels(i + 1);
				System.arraycopy(pixels, width * y, newpix, width * i, width);
			}
			fp1.setPixels(newpix);
			return;
		}

		if (ip instanceof ByteProcessor) {
			byte[] newpix = new byte[width * size];
			for (int i = 0; i < size; i++) {
				Object pixels = is.getPixels(i + 1);
				System.arraycopy(pixels, width * y, newpix, width * i, width);
			}
			fp1.setPixels(newpix);
			return;
		}

		if (ip instanceof FloatProcessor) {
			float[] newpix = new float[width * size];
			for (int i = 0; i < size; i++) {
				Object pixels = is.getPixels(i + 1);
				System.arraycopy(pixels, width * y, newpix, width * i, width);
			}
			fp1.setPixels(newpix);
			return;
		}

		if (ip instanceof ColorProcessor) {
			int[] newpix = new int[width * size];
			for (int i = 0; i < size; i++) {
				Object pixels = is.getPixels(i + 1);
				System.arraycopy(pixels, width * y, newpix, width * i, width);
			}
			fp1.setPixels(newpix);
			return;
		}
	}

	void updateZYView(Point p, ImageStack is) {
		int width = is.getWidth();
		int height = is.getHeight();
		int ds = is.getSize();
		ImageProcessor ip = is.getProcessor(1);
		int x = p.x;

		if (ip instanceof FloatProcessor) {
			float[] newpix = new float[ds * height];
			for (int i = 0; i < ds; i++) {
				float[] pixels = (float[]) is.getPixels(i + 1);// toFloatPixels(pixels);
				for (int y = 0; y < height; y++)
					newpix[i + y * ds] = pixels[x + y * width];
			}
			fp2.setPixels(newpix);
		}

		if (ip instanceof ByteProcessor) {
			byte[] newpix = new byte[ds * height];
			for (int i = 0; i < ds; i++) {
				byte[] pixels = (byte[]) is.getPixels(i + 1);// toFloatPixels(pixels);
				for (int y = 0; y < height; y++)
					newpix[i + y * ds] = pixels[x + y * width];
			}
			fp2.setPixels(newpix);
		}

		if (ip instanceof ShortProcessor) {
			short[] newpix = new short[ds * height];
			for (int i = 0; i < ds; i++) {
				short[] pixels = (short[]) is.getPixels(i + 1);// toFloatPixels(pixels);
				for (int y = 0; y < height; y++)
					newpix[i + y * ds] = pixels[x + y * width];
			}
			fp2.setPixels(newpix);
		}

		if (ip instanceof ColorProcessor) {
			int[] newpix = new int[ds * height];
			for (int i = 0; i < ds; i++) {
				int[] pixels = (int[]) is.getPixels(i + 1);// toFloatPixels(pixels);
				for (int y = 0; y < height; y++)
					newpix[i + y * ds] = pixels[x + y * width];
			}
			fp2.setPixels(newpix);
		}
	}
	// }}

	void dispose() {
		synchronized (this) {
			done = true;
			notify();
		}
		xy_stack.setOverlay(null);
		canvas.removeMouseListener(this);
		canvas.removeMouseMotionListener(this);
		canvas.removeKeyListener(this);
		canvas.setCustomRoi(false);
		xz_image.setOverlay(null);
		ImageWindow win1 = xz_image.getWindow();
		if (win1 != null) {
			win1.removeMouseWheelListener(this);
			ImageCanvas ic = win1.getCanvas();
			if (ic != null) {
				ic.removeKeyListener(this);
				ic.removeMouseListener(this);
				ic.removeMouseMotionListener(this);
				ic.setCustomRoi(false);
			}
		}
		xz_image.changes = false;
		xz_image.close();
		yz_image.setOverlay(null);
		ImageWindow win2 = yz_image.getWindow();
		if (win2 != null) {
			win2.removeMouseWheelListener(this);
			ImageCanvas ic = win2.getCanvas();
			if (ic != null) {
				ic.removeKeyListener(this);
				ic.removeMouseListener(this);
				ic.removeMouseMotionListener(this);
				ic.setCustomRoi(false);
			}
		}
		yz_image.changes = false;
		yz_image.close();
		ImagePlus.removeImageListener(this);

		win.removeWindowListener(this);
		win.removeMouseWheelListener(this);
		win.removeFocusListener(this);
		win.setResizable(true);
		instance = null;
		previousID = xy_stack.getID();
		previousX = crossLoc.x;
		previousY = crossLoc.y;
		imageStack = null;
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
		ImageCanvas xyCanvas = xy_stack.getCanvas();
		startingSrcRect = (Rectangle) xyCanvas.getSrcRect().clone();
		mouseDragged(e);
	}

	public void mouseDragged(MouseEvent e) {
		if (e.getSource().equals(canvas)) {
			crossLoc = canvas.getCursorLoc();
		} else if (e.getSource().equals(xz_image.getCanvas())) {
			crossLoc.x = xz_image.getCanvas().getCursorLoc().x;
			int pos = xz_image.getCanvas().getCursorLoc().y;
			int z = (int) Math.round(pos / az);
			int slice = z + 1;
			if (hyperstack)
				xy_stack.setPosition(xy_stack.getChannel(), slice, xy_stack.getFrame());
			else
				xy_stack.setSlice(slice);
		} else if (e.getSource().equals(yz_image.getCanvas())) {
			int pos;
			crossLoc.y = yz_image.getCanvas().getCursorLoc().y;
			pos = yz_image.getCanvas().getCursorLoc().x;

			int z = (int) Math.round(pos / az);
			if (hyperstack)
				xy_stack.setPosition(xy_stack.getChannel(), z + 1, xy_stack.getFrame());
			else
				xy_stack.setSlice(z + 1);
		}

		update();
	}

	public void mouseReleased(MouseEvent e) {
		ImageCanvas ic = xy_stack.getCanvas();
		Rectangle srcRect = ic.getSrcRect();
		if (srcRect.x != startingSrcRect.x || srcRect.y != startingSrcRect.y) {
			// user has scrolled xy image
			int dy = srcRect.y - startingSrcRect.y;
			ImageCanvas yzic = yz_image.getCanvas();
			Rectangle yzSrcRect = yzic.getSrcRect();

			yzSrcRect.y += dy;
			if (yzSrcRect.y < 0)
				yzSrcRect.y = 0;
			if (yzSrcRect.y > yz_image.getHeight() - yzSrcRect.height)
				yzSrcRect.y = yz_image.getHeight() - yzSrcRect.height;

			yzic.repaint();
			int dx = srcRect.x - startingSrcRect.x;
			ImageCanvas xzic = xz_image.getCanvas();
			Rectangle xzSrcRect = xzic.getSrcRect();
			xzSrcRect.x += dx;
			if (xzSrcRect.x < 0)
				xzSrcRect.x = 0;
			if (xzSrcRect.x > xz_image.getWidth() - xzSrcRect.width)
				xzSrcRect.x = xz_image.getWidth() - xzSrcRect.width;
			xzic.repaint();
		}
	}

	/** Refresh the output windows. */
	synchronized void update() {
		notify();
	}

	private void exec() {
		if (canvas == null)
			return;
		int width = xy_stack.getWidth();
		int height = xy_stack.getHeight();
		if (hyperstack) {
			int mode = IJ.COMPOSITE;
			if (xy_stack.isComposite()) {
				mode = ((CompositeImage) xy_stack).getMode();
				if (mode != currentMode)
					imageStack = null;
			}
			if (imageStack != null) {
				int c = xy_stack.getChannel();
				int t = xy_stack.getFrame();
				if ((mode != IJ.COMPOSITE && c != currentChannel) || t != currentFrame)
					imageStack = null;
			}
		}
		ImageStack is = imageStack;
		if (is == null)
			is = imageStack = getStack();
		double arat = az / ax;
		double brat = az / ay;

		Point p = crossLoc;
		if (p.y >= height)
			p.y = height - 1;
		if (p.x >= width)
			p.x = width - 1;
		if (p.x < 0)
			p.x = 0;
		if (p.y < 0)
			p.y = 0;

		updateViews(p, is);

		if (!done) {
			clearOverlays();
			updateOverlayCrosses(p, arat, brat);
			updateOverlayPoints(arat, brat);
			repaintImages();
		}

		canvas.setCustomRoi(true);

		arrangeWindows(sticky);
		initialized = true;
	}

	private void clearOverlays() {
		xy_overlay.clear();
		xz_overlay.clear();
		yz_overlay.clear();
	}

	private void repaint(ImagePlus ip) {
		ip.getCanvas().repaint();
	}

	private void repaintImages() {
		repaint(xy_stack);
		repaint(xz_image);
		repaint(yz_image);
	}

	void drawOverlayCross(ImagePlus ip, Overlay ov, float x, float y) {
		int width = ip.getWidth();
		int height = ip.getHeight();
		Line l = new Line(0f, y, width, y);
		l.setStrokeColor(Color.RED);
		ov.add(l);
		l = new Line(x, 0f, x, height);
		l.setStrokeColor(Color.RED);
		ov.add(l);
	}

	private void updateOverlayCrosses(Point p, double arat, double brat) {
		int x = p.x;
		int y = p.y;

		drawOverlayCross(xy_stack, xy_overlay, p.x, p.y);

		int zlice = xy_stack.getSlice() - 1;
		int zcoord = (int) Math.round(arat * zlice);
		drawOverlayCross(xz_image, xz_overlay, x, zcoord);

		zcoord = (int) Math.round(brat * zlice);
		drawOverlayCross(yz_image, yz_overlay, zcoord, y);

		IJ.showStatus(xy_stack.getLocationAsString(crossLoc.x, crossLoc.y));
	}

	private void updateOverlayPoints(double arat, double brat) {
		int x = crossLoc.x;
		int y = crossLoc.y;
		int z = xy_stack.getSlice() - 1;
		int t = xy_stack.getFrame() - 1;
		int showdist = 5;
		int ovalSize = 16;
		ArrayList<Point4D> currentList = annotationLists.get(t);

		double piover2 = Math.PI / 2.0;

		for (Point4D p : currentList) {
			// XY
			if (Math.abs(p.z - z) <= showdist) {
				double dist = ovalSize * Math.cos((Math.abs(p.z - z) / (float) showdist) * piover2);
				OvalRoi o1 = new OvalRoi(p.x - dist / 2, p.y - dist / 2, dist, dist);
				o1.setStrokeColor(Color.GREEN);
				xy_overlay.add(o1);

				if (p.z == z) {
					Line l1 = new Line(p.x - 3, p.y, p.x + 3, p.y);
					l1.setStrokeColor(Color.GREEN);
					xy_overlay.add(l1);
					Line l2 = new Line(p.x, p.y - 3, p.x, p.y + 3);
					l2.setStrokeColor(Color.GREEN);
					xy_overlay.add(l2);
				}
			}

			// YZ
			if (Math.abs(p.x - x) <= showdist * brat) {
				int zcoord = (int) Math.round(brat * p.z);

				double dist = ovalSize * Math.cos((Math.abs(p.x - x) / (float) showdist) / (brat / 1.5f) * piover2);
				OvalRoi o1 = new OvalRoi(zcoord - dist / 2, p.y - dist / 2, dist, dist);
				o1.setStrokeColor(Color.GREEN);
				yz_overlay.add(o1);

				if (p.x == x) {
					Line l1 = new Line(zcoord, p.y - 3, zcoord, p.y + 3);
					l1.setStrokeColor(Color.GREEN);
					yz_overlay.add(l1);
					Line l2 = new Line(zcoord - 3, p.y, zcoord + 3, p.y);
					l2.setStrokeColor(Color.GREEN);
					yz_overlay.add(l2);
				}
			}

			// XZ
			if (Math.abs(p.y - y) <= showdist * arat) {
				int zcoord = (int) Math.round(arat * p.z);

				double dist = ovalSize * Math.cos((Math.abs(p.y - y) / (float) showdist) / (arat / 1.5f) * piover2);
				OvalRoi o1 = new OvalRoi(p.x - dist / 2, zcoord - dist / 2, dist, dist);
				o1.setStrokeColor(Color.GREEN);
				xz_overlay.add(o1);

				if (p.y == y) {
					Line l1 = new Line(p.x - 3, zcoord, p.x + 3, zcoord);
					l1.setStrokeColor(Color.GREEN);
					xz_overlay.add(l1);
					Line l2 = new Line(p.x, zcoord - 3, p.x, zcoord + 3);
					l2.setStrokeColor(Color.GREEN);
					xz_overlay.add(l2);
				}
			}
		}
	}

	public void mouseMoved(MouseEvent e) {
	}

	private double LengthSquared(float x1, float y1, float z1, float x2, float y2, float z2) {
		return Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2) + Math.pow(z1 - z2, 2);
	}

	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_ESCAPE) {
			IJ.beep();
			dispose();
		} else if (key == KeyEvent.VK_SPACE) {
			int x = crossLoc.x;
			int y = crossLoc.y;
			int z = xy_stack.getSlice() - 1;
			int t = xy_stack.getFrame() - 1;
			annotationLists.get(t).add(new Point4D(x, y, z, t));
			update();
		} else if (key == KeyEvent.VK_DELETE) {
			int x = crossLoc.x;
			int y = crossLoc.y;
			int z = xy_stack.getSlice() - 1;
			int t = xy_stack.getFrame() - 1;
			int index = -1;
			ArrayList<Point4D> currentPoints = annotationLists.get(t);
			double minDist = Double.MAX_VALUE;
			for (int i = 0; i < currentPoints.size(); i++) {
				Point4D p = currentPoints.get(i);
				double distSquared = LengthSquared(p.x, p.y, p.z, x, y, z);
				if (distSquared < minDist) {
					index = i;
					minDist = distSquared;
				}
			}

			if (index != -1 && minDist < 100 /* 10px squared radius */) {
				currentPoints.remove(index);
				update();
			}
		} else if (IJ.shiftKeyDown()) {
			int width = xy_stack.getWidth(), height = xy_stack.getHeight();
			switch (key) {
			case KeyEvent.VK_LEFT:
				crossLoc.x--;
				if (crossLoc.x < 0)
					crossLoc.x = 0;
				break;
			case KeyEvent.VK_RIGHT:
				crossLoc.x++;
				if (crossLoc.x >= width)
					crossLoc.x = width - 1;
				break;
			case KeyEvent.VK_UP:
				crossLoc.y--;
				if (crossLoc.y < 0)
					crossLoc.y = 0;
				break;
			case KeyEvent.VK_DOWN:
				crossLoc.y++;
				if (crossLoc.y >= height)
					crossLoc.y = height - 1;
				break;
			default:
				return;
			}
			update();
		}
	}

	public void keyReleased(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
	}

	public void actionPerformed(ActionEvent ev) {
	}

	public void imageClosed(ImagePlus imp) {
		if (!done)
			dispose();
	}

	public void imageOpened(ImagePlus imp) {
	}

	public void imageUpdated(ImagePlus imp) {
		if (imp == this.xy_stack) {
			ImageProcessor ip = imp.getProcessor();
			min = ip.getMin();
			max = ip.getMax();
			update();
		}
	}

	// {{ Window Events
	public void windowActivated(WindowEvent e) {
		arrangeWindows(sticky);
	}

	public void windowClosed(WindowEvent e) {
	}

	public void windowClosing(WindowEvent e) {
		if (!done)
			dispose();
	}

	public void windowDeactivated(WindowEvent e) {
	}

	public void windowDeiconified(WindowEvent e) {
		arrangeWindows(sticky);
	}

	public void windowIconified(WindowEvent e) {
	}

	public void windowOpened(WindowEvent e) {
	}

	public void adjustmentValueChanged(AdjustmentEvent e) {
		update();
	}

	public void focusGained(FocusEvent e) {
		ImageCanvas ic = xy_stack.getCanvas();
		if (ic != null)
			canvas.requestFocus();
		arrangeWindows(sticky);
	}

	public void focusLost(FocusEvent e) {
		arrangeWindows(sticky);
	}

	// }}

	public void mouseWheelMoved(MouseWheelEvent e) {
		if (e.getSource().equals(xz_image.getWindow())) {
			crossLoc.y += e.getWheelRotation();
		} else if (e.getSource().equals(yz_image.getWindow())) {
			crossLoc.x += e.getWheelRotation();
		}
		update();
	}

	public static ImagePlus getImage() {
		if (instance != null)
			return instance.xy_stack;
		else
			return null;
	}

	public static int getImageID() {
		ImagePlus img = getImage();
		return img != null ? img.getID() : 0;
	}

	public static void stop() {
		if (instance != null)
			instance.dispose();
	}

	public static synchronized boolean isOrthoViewsImage(ImagePlus imp) {
		if (imp == null || instance == null)
			return false;
		else
			return imp == instance.xy_stack || imp == instance.xz_image || imp == instance.yz_image;
	}

	public static Orthogonal_Views getInstance() {
		return instance;
	}

	public int[] getCrossLoc() {
		int[] loc = new int[3];
		loc[0] = crossLoc.x;
		loc[1] = crossLoc.y;
		loc[2] = xy_stack.getSlice() - 1;
		return loc;
	}

	public void setCrossLoc(int x, int y, int z) {
		crossLoc.setLocation(x, y);
		int slice = z + 1;
		if (slice != xy_stack.getSlice()) {
			if (hyperstack)
				xy_stack.setPosition(xy_stack.getChannel(), slice, xy_stack.getFrame());
			else
				xy_stack.setSlice(slice);
			sliceSet = true;
		}
		while (!initialized) {
			IJ.wait(10);
		}
		update();
	}

	public ImagePlus getXZImage() {
		return xz_image;
	}

	public ImagePlus getYZImage() {
		return yz_image;
	}

	public void run() {
		while (!done) {
			synchronized (this) {
				try {
					wait();
				} catch (InterruptedException e) {
				}
			}
			if (!done)
				exec();
		}
	}
	
	public void drawSphere(ImageStack is, int cx, int cy, int cz, float rx, float ry, float rz) {
        // Diameter and bounds
		int dx = (int)Math.round(rx*2);
        int dy = (int)Math.round(ry*2);
        int dz = (int)Math.round(rz*2);
        int xmin=(int)(cx-rx+0.5), ymin=(int)(cy-ry+0.5), zmin=(int)(cz-rz+0.5);
        int xmax=xmin+dx, ymax=ymin+dy, zmax=zmin+dz;
        
        // Radii shortcuts
        float rxy2 = rx * rx * ry * ry;
        float rxz2 = rx * rx * rz * rz;
        float ryz2 = ry * ry * rz * rz;
        float rxyz2 = rx * rx * ryz2;
           
        for (int x=xmin; x<=xmax; x++) {
            for (int y=ymin; y<=ymax; y++) {
                for (int z=zmin; z<=zmax; z++) {
                	int px = x - cx;
            		int py = y - cy;
            		int pz = z - cz;
                	
            		// Is point inside ellipse?
                    if (( ryz2 * px * px ) + ( rxz2 * py * py ) + ( rxy2 * pz * pz ) <= rxyz2)
                        is.setVoxel(x, y, z, 255);
                }
            }
        }
    }

	public String commandExecuting(String command) {
		if (command.equals("Save")) {
			ResultsTable rt = new ResultsTable();

			for (Map.Entry<Integer, ArrayList<Point4D>> lst : annotationLists.entrySet()) {
				int t = lst.getKey();
				ArrayList<Point4D> currentList = lst.getValue();
				for (Point4D p : currentList) {
					rt.incrementCounter();
					rt.addValue("x", Integer.toString(p.x));
					rt.addValue("y", Integer.toString(p.y));
					rt.addValue("z", Integer.toString(p.z));
					rt.addValue("t", Integer.toString(t));
				}
			}

			SaveDialog sd = new SaveDialog("Save Pointset", "pointset", ".csv");
			rt.save(sd.getDirectory() + sd.getFileName());

			return null;
		} else if (command.equals("Open...")) {
			OpenDialog od = new OpenDialog("Open Pointset");
			ResultsTable rt = null;

			try {
				rt = ResultsTable.open(od.getPath());
			} catch (IOException e) {
				IJ.log("Could not open " + od.getPath());
			}

			if (rt != null) {
				for (int i = 0; i < rt.size(); i++) {
					int x = (int) rt.getValue("x", i);
					int y = (int) rt.getValue("y", i);
					int z = (int) rt.getValue("z", i);
					int t = (int) rt.getValue("t", i);
					Point4D currentPoint = new Point4D(x, y, z, t);
					annotationLists.get(t).add(currentPoint);
				}
				update();
			}
			return null;
		} else if (command.equals("Measure")) {
			IJ.log(Integer.toString(xy_stack.getBitDepth()));
			IJ.log(Integer.toString(xy_stack.getNChannels()));

			ImagePlus ts2 = IJ.createHyperStack("ts2", xy_stack.getWidth(), xy_stack.getHeight(), 1,
					xy_stack.getNSlices(), xy_stack.getNFrames(), xy_stack.getBitDepth());
			
			float baseRadius = 6f;
			ImageStack ts2stack = ts2.getStack();
			for (Map.Entry<Integer, ArrayList<Point4D>> lst : annotationLists.entrySet()) {
				int t = lst.getKey();
				ArrayList<Point4D> currentList = lst.getValue();
				for (Point4D p : currentList) {
					drawSphere(ts2stack, p.x, p.y, ts2.getStackIndex(1, p.z, t), baseRadius, baseRadius, (float)(baseRadius / az));
				}
			}
			
			ImageMerge im = new ImageMerge();
			ImagePlus merged = im.run(xy_stack, ts2);
			merged.show();

			return null;
		}

		return command;
	}
}
